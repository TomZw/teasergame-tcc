﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Start()
        {
            return View();
        }

        public ActionResult Hub()
        {
            return View();
        }

        public ActionResult Index(int id)
        {
            ViewBag.id = id;
            return View();
        }

        public ActionResult CreditLimitProblem(Models.CreditDecisionModel model)
        {
            var choice = "";
            if (model.choice != null)
            {
                choice = model.choice;
            }
            var FinancingNeeds = model.FinancingNeeds;
            var SalesPrice = model.SalesPrice;
            var InsuranceCosts = model.InsuranceCosts;

            var RealFinancingNeeds = "";
            var RealSalesPrice = "";
            var RealInsuranceCosts = "";

            switch (choice)
            {
                case "A":
                    {
                        RealFinancingNeeds = "Increase";
                        RealSalesPrice = "Decrease";
                        RealInsuranceCosts = "Equal";
                        break;
                    }
                case "B":
                    {
                        RealFinancingNeeds = "Increase";
                        RealSalesPrice = "Equal";
                        RealInsuranceCosts = "Increase";
                        break;
                    }
                default:
                    {
                        RealFinancingNeeds = "Decrease";
                        RealSalesPrice = "Decrease";
                        RealInsuranceCosts = "Decrease";
                        break;
                    }
            }

            var m = new Models.CreditFeedbackModel();
                m.choice = choice;
                m.FinancingNeeds = FinancingNeeds;
                m.SalesPrice = SalesPrice;
                m.InsuranceCosts = InsuranceCosts;
                m.RealFinancingNeeds = RealFinancingNeeds;
                m.RealSalesPrice = RealSalesPrice;
                m.RealInsuranceCosts = RealInsuranceCosts;

            return View(m);
        }

        public ActionResult AlignmentProblem(Models.AlignmentDecisionModel model)
        {
            int[] choice = new int[0];
            if (model.choice != null)
            {
                choice = model.choice;
            }
            int[] answers = new int[] { 2, 3, 6 };
            List<int> Correct = new List<int>();
            List<int> False = new List<int>();
            List<int> Missed = new List<int>();

            if (choice.Length == 0)
            {
                return View();
            }
            for (int i = 0;i < choice.Length; i++)
            {
                if (answers.Contains(choice[i]))
                {
                    Correct.Add(choice[i]);
                }
                else
                {
                    False.Add(choice[i]);
                }
            }

            for (int i = 0;i < answers.Length; i++)
            {
                if (!(choice.Contains(answers[i])))
                {
                    Missed.Add(answers[i]);
                }
            }

            var m = new Models.AlignmentFeedbackModel();

            m.choice = choice;
            m.answers = answers;
            m.Correct = Correct;
            m.False = False;
            m.Missed = Missed;

            return View(m);
        }

        public ActionResult ProductionProblem(Models.ProductionDecisionModel model)
        {
            NumberFormatInfo p = new NumberFormatInfo();
            p.NumberDecimalSeparator = ".";

            double[] SafetyStock = new double[] { Convert.ToDouble(model.SafetyStockLemon,p), Convert.ToDouble(model.SafetyStockCare,p), Convert.ToDouble(model.SafetyStockCondition,p) };
            int[] ProductionInterval = new int[] { model.ProductionIntervalLemon, model.ProductionIntervalCare, model.ProductionIntervalCondition };

            //CoolClassic Lemon
            double[,,] Costs = new double[3, 11, 10] {{ { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 12934, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } },
                                             
                                                        //CoolCare
                                                    {   { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 18756, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } },
                                            
                                                    //CoolCondition
                                                    {   { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 20014, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } } };

            double[] OptimalCost = new double[] { 12934, 14367, 15679 };


            //CoolClassic Lemon
            double[,,] ServiceLevels = new double[3, 11, 10] { {{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 97, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } },

                                                                //CoolCare
                                                              { { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 92, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } },

                                                                //CoolCondition
                                                              { { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 96, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                                                                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } } };


            double[] AchievedCost = new double[3];
            double[] AchievedServiceLevel = new double[3];
            Boolean[] AdequateService = new bool[3];
            Boolean[] CostMinimized = new bool[3];
            ViewBag.test = model.SafetyStockCare;

            for (int i = 0; i < 3; i++)
            {
                AchievedCost[i] = Costs[i, Convert.ToInt32(2 * SafetyStock[i]), ProductionInterval[i] - 1];
                AchievedServiceLevel[i] = ServiceLevels[i, Convert.ToInt32(2 * SafetyStock[i]), ProductionInterval[i] - 1];
                AdequateService[i] = (AchievedServiceLevel[i] >= 95.0);
                if (AdequateService[i])
                {
                    CostMinimized[i] = (AchievedCost[i] == OptimalCost[i]);
                }
            }

            var m = new Models.ProductionFeedbackModel();
            m.Cost = AchievedCost;
            m.ServiceLevel = AchievedServiceLevel;
            m.AdequateService = AdequateService;
            m.CostMinimized = CostMinimized;

            return View(m);
        }

        public ActionResult PostponementProblem(Models.ReliabilityDecisionModel model)
        {
            var ProductCosts = model.ProductCosts;
            var Stock = model.Stock;
            var FinancingNeeds = model.FinancingNeeds;



            return View();
        }

    }
}