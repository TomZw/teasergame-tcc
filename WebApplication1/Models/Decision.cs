﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class CreditDecisionModel
    {
        public string choice {get;set;}
        public string FinancingNeeds { get; set; }
        public string SalesPrice { get; set; }
        public string InsuranceCosts { get; set; }
    }

    public class CreditFeedbackModel
    {
        public string choice { get; set; }
        public string FinancingNeeds { get; set; }
        public string SalesPrice { get; set; }
        public string InsuranceCosts { get; set; }
        public string RealFinancingNeeds { get; set; }
        public string RealSalesPrice { get; set; }
        public string RealInsuranceCosts { get; set; }
    }

    public class AlignmentDecisionModel
    {
        public int[] choice { get; set; }
    }

    public class AlignmentFeedbackModel
    {
        public int[] choice { get; set; }
        public int[] answers { get; set; }
        public List<int> Correct { get; set; }
        public List<int> False { get; set; }
        public List<int> Missed { get; set; }
    }

    public class ProductionDecisionModel
    {
        public String SafetyStockLemon { get; set; }
        public int ProductionIntervalLemon { get; set; }
        public String SafetyStockCare { get; set; }
        public int ProductionIntervalCare { get; set; }
        public String SafetyStockCondition { get; set; }
        public int ProductionIntervalCondition { get; set; }
    }

    public class ProductionFeedbackModel
    {
        public double[] Cost { get; set; }
        public double[] ServiceLevel { get; set; }
        public Boolean[] AdequateService { get; set; }
        public Boolean[] CostMinimized { get; set; }

    }

    public class ReliabilityDecisionModel
    {
        public String ProductCosts { get; set; }
        public String Stock { get; set; }
        public String FinancingNeeds { get; set; }
    }

    public class ReliabilityFeedbackModel
    {

    }
}